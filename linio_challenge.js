/*
Linio challenge:

Lenguaje Javascript, testeado en consola de chrome 78.

3 alternativas:
    1- Sin if
    2- Separo la selección del mensaje en una función. Unit tests y explciación de cómo se podría llevar a OOP
    3- Simple y sin meter mucha ingenieria.

1- 
Sin if, espacio en disco N, complejidad algoritmica O(24/15N).

Se prueba con salida en pantalla dado que no hay separación de responsabilidades.


----- INICIO CÓDIGO

res = [0];
for (i=1;i <=100;i++){
	res[i] = i;
}

for (i=3;i <=100;i+=3){
	res[i] = "Linio";
}

for (i=5;i <=100;i+=5){
	res[i] = "IT";
}

for (i=15;i <=100;i+=15){
	res[i] = "Linianos";
}

for (i=1;i <=100;i++){
	console.log(res[i]);
}

----- FIN CÓDIGO

-----------------------------------------------------------------------------

2-
Separo la selección del mensaje a imprimir. Se usa un switch, esta permitido? El switch tiene sus múltiples branches. Se testea selección de mensaje por unit test e integración por salida en consola. Complejidad O(N)

----- INICIO CÓDIGO

function obtenerMensaje(i){
	posiblesMensajes = {
		0: i,
		1: "Linio",
		2: "IT",
		7: "Linianos"
	};
	
	mensaje = "";
	esMultiploDeTres = !(i % 3) << 0;
	esMultiploDeCinco = !(i % 5) << 1;
	esMultiploDeTresYCinco = !(i % (3 * 5)) << 2;
	
	selector = 0 + esMultiploDeTres + esMultiploDeCinco + esMultiploDeTresYCinco;
	
	mensaje = posiblesMensajes[selector];
	
	return mensaje;
}

for (i=1;i <=100;i++){
	console.log(obtenerMensaje(i));
}

//Llevado a OOP el selector de mensajes podría estar dentro de un factory que dependiendo qué numero es y, por quienes es divisible, devuelve una instancia de una clase particular ej.: "NumeroDivisiblePorTres" que extiende de una clase "NumeroChallenge" que mediante un método abstracto obligla a sus subclases implementar un imprimir, por constructor reciben el número correspondiente. 

//Test Cases - Se puede mejorar agregando descripción a cada test, agregando los valores obtenidos en la salida (por si hay error). El OK y el FAIL son para visualziar rápidamente si esta OK o no el test. Se peude agregar un framework basicamente :)

//testeo casoe base 3, 5, 15,4, extremos 1 y 100, medios 99. No testeo casos fuera del universo.

console.log(obtenerMensaje(1) == "1" ? "OK 1 no es múltiplo de 3 ni 5 y se obtiene 1" : "FAIL 1 no es múltiplo de 1 ni 1 y no se obtuvo 1");
console.log(obtenerMensaje(3) == "Linio" ? "OK 3 es múltiplo de 3 y se obtiene Linio" : "FAIL 3 es múltiplo de 3 y no se obtuvo Linio");
console.log(obtenerMensaje(4) == "4" ? "OK 4 no es múltiplo de 3 ni 5 y se obtiene 4" : "FAIL 4 no es múltiplo de 3 ni 5 y no se obtuvo 4");
console.log(obtenerMensaje(5) == "IT" ? "OK 5 es múltiplo de 5 y se obtiene IT" : "FAIL 5 es múltiplo de 5 y no se obtuvo IT");
console.log(obtenerMensaje(15) == "Linianos" ? "OK 15 es múltiplo de 3 y 5 y se obtiene Linianos" : "FAIL 15 es es múltiplo de 3 y 5 y no se obtuvo Linianos");
console.log(obtenerMensaje(99) == "Linio" ? "OK 99 es múltiplo de 3 y se obtiene Linio" : "FAIL 99 es múltiplo de 3 y no se obtuvo Linio");
console.log(obtenerMensaje(100) == "IT" ? "OK 100 es múltiplo de 5 y se obtiene IT" : "FAIL 100 es múltiplo de 5 y no se obtuvo IT");

----- FIN CÓDIGO

-----------------------------------------------------------------------------

3-
Sin muchas vueltas, un array prearmado y se recorre. Si la solución no va a escalar, no necesita mucha ingenieria. Se prueba con impresión en pantalla dado que no hay separación de responsabilidades.

Sin if, espacio en disco N, complejidad algoritmica O(N).

----- INICIO CÓDIGO

res = [
	"1",
	"2",
	"Linio",
	"4",
	"IT",
	"Linio",
	"7",
	...
	"14",
	"Linianos",
	"16",
	"17",
	"Linio",
	...
	"98",
	"Linio",
	"IT"
]

for (i=1;i <=100;i++){
	console.log(res[i]);
}

----- FIN CÓDIGO

*/
